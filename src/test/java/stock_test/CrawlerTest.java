package stock_test;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import stock.crawler.HttpClientService;
import stock.crawler.dataFormat.StockInfoFormat;
import stock.entity.CrawledTWStock;
import stock.repos.TWStockDAO;

public class CrawlerTest {
	
	@Test//final:匯入DB中
	public void test6() {
		
		//爬到的結果的存放處
		List<String[]> list = new ArrayList<>();
		
		//要擷取的頁面
		String url = "https://mops.twse.com.tw/mops/web/t05st03";
		//參數名稱
		Object[] params = new Object[] {"encodeURIComponent","step","firstin","off","keyword4","code1","TYPEK2"
				,"checkbtn","queryName","inpuType","TYPEK","co_id"};
		
		String co_id = "";
		int i;//1101
		//4142
		for(i=4000; i<5000; i++) {//公司電腦執行到2800-3000、家:已執行8500-9000
			
			try {
				System.out.println("delay@@");
				Thread.sleep(6000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			co_id = i + "";
			System.out.print("======= "+ co_id + " =======");
			
			//參數值
			Object[] values = new Object[] {"1","1","true","1","","","","","co_id","co_id","all",co_id};
			//參數對象
			List<NameValuePair> paramsList = HttpClientService.getParams(params, values);
			
			//發送post
			String result = null;
			try {
				result = HttpClientService.sendHttpsPost(url,paramsList);
			} catch (KeyManagementException e) {
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			
//			if(i==4141) {
//				System.out.println(result);
//				System.out.println("------------------------------------------------------");
//			}
			
			String[] stock_info;
			//判斷該公司代號存不存在
			if(result.replaceAll(" ", "").contains("不存在") || result.contains("公開發行公司不繼續公開發行") 
					|| result.contains("該公司基本資料無須揭露") || result.contains("上市公司已下市") 
					|| result.contains("公司代號不存在或公司未申報基本資料") || result.contains("已下櫃") ) {
				System.out.println("該代號 無 公司存在");
			}else {
				stock_info = new StockInfoFormat(result).removeUselessParagraph();
				list.add(stock_info);
			}
		}
		
		String config = "spring-mvc.xml";
		ApplicationContext ac = new ClassPathXmlApplicationContext(config);
		TWStockDAO dao = ac.getBean("twstockDAO",TWStockDAO.class);
		System.out.println(dao);
		
		//時間
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date today = new Date();
		String date = sdf.format(today);
//		CrawledTWStock ctw = new CrawledTWStock(arr[0],arr[1],arr[2],arr[3],arr[4],arr[5],arr[6],arr[7],
//				arr[8],arr[9],arr[10],arr[11],arr[12],date);
		
		//驗證是否成功
		for(String[] arr: list) {
			for(int li=0; li<arr.length; li++) {
				System.out.print(arr[li]+"、");
			}
			System.out.println("；");
		}
		
		CrawledTWStock ctw ;
		for(String[] arr: list) {
			ctw = new CrawledTWStock(arr[0],arr[1],arr[2],arr[3],arr[4],arr[5],arr[6],arr[7],
					arr[8],arr[9],arr[10],arr[11],arr[12],date);
			String result = dao.save(ctw);
			System.out.println("=====> result；"+ arr[1] +result);
		}
	}
	
	@Test//測試抽換成變數、變數自增=>可以成功，但會因為查詢過於頻繁而無法持續=>利用Thread.sleep
	public void test5() {
		
		//爬到的結果的存放處
		List<String[]> list = new ArrayList<>();
		
		//要擷取的頁面
		String url = "https://mops.twse.com.tw/mops/web/t05st03";
		//參數名稱
		Object[] params = new Object[] {"encodeURIComponent","step","firstin","off","keyword4","code1","TYPEK2"
				,"checkbtn","queryName","inpuType","TYPEK","co_id"};
		
		String co_id = "";
		int i;//1101
		for(i=2754; i<2755; i++) {
			
			try {
				System.out.println("delay@@");
				Thread.sleep(6000);
//				System.out.print("delay-over@@");
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			co_id = i + "";
			System.out.print("======= "+ co_id + " =======");
			
			//參數值
			Object[] values = new Object[] {"1","1","true","1","","","","","co_id","co_id","all",co_id};
			//參數對象
			List<NameValuePair> paramsList = HttpClientService.getParams(params, values);
			
			//發送post
			String result = null;
			try {
				result = HttpClientService.sendHttpsPost(url,paramsList);
			} catch (KeyManagementException e) {
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
//			if(i==2754) {
//				System.out.println(result);
//				System.out.println("------------------------------------------------------");
//			}
			
			String[] stock_info;
			//判斷該公司代號存不存在
//			if(result.replaceAll(" ", "").contains("公司不存在")) {
			if(result.replaceAll(" ", "").contains("公司不存在") || result.contains("公開發行公司不繼續公開發行") 
					|| result.contains("該公司基本資料無須揭露") || result.contains("上市公司已下市") 
					|| result.contains("公司代號不存在或公司未申報基本資料") ) {
				System.out.println("該代號 無 公司存在");
			}else {
				stock_info = new StockInfoFormat(result).removeUselessParagraph();
				list.add(stock_info);
			}
		}
		
		//驗證是否成功
		/*
		for(String[] arr: list) {
			for(int li=0; li<arr.length; li++) {
				System.out.print(arr[li]+"、");
			}
			System.out.println("；");
		}
		*/
	}
	
	
	@Test//測試抽換成變數、變數自增=>可以成功，但會因為查詢過於頻繁而無法持續=>利用Thread.sleep
	public void test4() {
		
		//要擷取的頁面
		String url = "https://mops.twse.com.tw/mops/web/t05st03";
		//參數名稱
		Object[] params = new Object[] {"encodeURIComponent","step","firstin","off","keyword4","code1","TYPEK2"
				,"checkbtn","queryName","inpuType","TYPEK","co_id"};
		
		String co_id = "";
		int i;
		for(i=1101; i<1501; i++) {
			
			try {
				System.out.print("delay@@");
				Thread.sleep(7000);
				System.out.print("delay-over@@");
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			co_id = i + "";
			System.out.println("======= "+ co_id + " =======");
			
			//參數值
			Object[] values = new Object[] {"1","1","true","1","","","","","co_id","co_id","all",co_id};
			//參數對象
			List<NameValuePair> paramsList = HttpClientService.getParams(params, values);
			
			//發送post
			String result = null;
			try {
				result = HttpClientService.sendHttpsPost(url,paramsList);
			} catch (KeyManagementException e) {
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			//判斷該公司代號存不存在
			if(result.replaceAll(" ", "").contains("公司不存在")) {
				System.out.println("該代號 無 公司存在");
			}else {
				new StockInfoFormat(result).removeUselessParagraph();
			}
			
		}
	}
	
	@Test//測試公司不存在的訊息樣子
	public void test3() {
		//要擷取的頁面
		String url = "https://mops.twse.com.tw/mops/web/t05st03";
		//參數名稱
		Object[] params = new Object[] {"encodeURIComponent","step","firstin","off","keyword4","code1","TYPEK2"
				,"checkbtn","queryName","inpuType","TYPEK","co_id"};
		
		//參數值
		Object[] values = new Object[] {"1","1","true","1","","","","","co_id","co_id","all","0"};
		//參數對象
		List<NameValuePair> paramsList = HttpClientService.getParams(params, values);
		
		//發送post
		String result = null;
		try {
			result = HttpClientService.sendHttpsPost(url,paramsList);
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//判斷該公司代號存不存在
		if(result.replaceAll(" ", "").contains("<center><h3>0之公司不存在！</h3>")) {
			System.out.println("該代號 無 公司存在");
		}
	}
	
	
	@Test//成功爬https://mops.twse.com.tw/mops/web/t05st03，獲取公司基本資訊
	public void test2() {
		//要擷取的頁面
		String url = "https://mops.twse.com.tw/mops/web/t05st03";
		//參數名稱
		Object[] params = new Object[] {"encodeURIComponent","step","firstin","off","keyword4","code1","TYPEK2"
				,"checkbtn","queryName","inpuType","TYPEK","co_id"};
		//參數值
		Object[] values = new Object[] {"1","1","true","1","","","","","co_id","co_id","all","1201"};
		//參數對象
		List<NameValuePair> paramsList = HttpClientService.getParams(params, values);
//		System.out.println(paramsList);
		
		//發送post
		String result = null;
		try {
			result = HttpClientService.sendHttpsPost(url,paramsList);
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
//		System.out.println(result);
		
		new StockInfoFormat(result).removeUselessParagraph();
	}
	
	
	/**
	 * 錯誤訊息 : 
	 * PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException: 
	 * unable to find valid certification path to requested target
	 */
	@Test
	public void test1() {
		//要擷取的頁面
		String url = "https://mops.twse.com.tw/mops/web/t05st03";
		
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost hPost = new HttpPost(url);
		CloseableHttpResponse response = null;
		List<NameValuePair> parms = new ArrayList<NameValuePair>();
		parms.add(new BasicNameValuePair("encodeURIComponent","1"));
		parms.add(new BasicNameValuePair("step","1"));
		parms.add(new BasicNameValuePair("firstin","true"));
		parms.add(new BasicNameValuePair("off","1"));
		parms.add(new BasicNameValuePair("keyword4",""));
		parms.add(new BasicNameValuePair("code1",""));
		parms.add(new BasicNameValuePair("TYPEK2",""));
		parms.add(new BasicNameValuePair("checkbtn",""));
		parms.add(new BasicNameValuePair("queryName","co_id"));
		parms.add(new BasicNameValuePair("inpuType","co_id"));
		parms.add(new BasicNameValuePair("TYPEK","all"));
		parms.add(new BasicNameValuePair("co_id","1201"));
		
		try {
			hPost.setEntity(new UrlEncodedFormEntity(parms));
			response = client.execute(hPost);
			client.close();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
