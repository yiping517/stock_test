package stock_test;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import stock.crawler.HttpClientService;
import stock.crawler.dataFormat.AllCompanyMonthlyRevenuePage;

//爬取頁面:"https://mops.twse.com.tw/nas/t21/sii/t21sc03_109_8_0.html"
public class CrawlerMonthlyRevenueAllTWStock {
	
	private static final Logger logger = LogManager.getLogger(CrawlerMonthlyRevenueAllTWStock.class);
	private String url = "https://mops.twse.com.tw/nas/t21/sii/t21sc03_#@_0.html";
	
	@Test
	public void getLatestInfo() {
		//時間設定
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-d");
		String today = sdf.format(d);
		int day = Integer.valueOf(today.substring(today.lastIndexOf("-")+1));
		logger.info(day);
		if(day>10) {
			
		}
//		url = url.replace("#@", time);
		logger.info(url);
	}
	
	@Test
	public void getHistoryInfo() {
		
		//月份array
		int[] months = {1,2,3,4,5,6,7,8,9,10,11,12};
		
		//時間設定
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-d");
		String today = sdf.format(d);
		int endYear = Integer.valueOf(today.substring(0, today.indexOf("-")))-1911;
		int month = Integer.valueOf(today.substring(today.indexOf("-")+1, today.lastIndexOf("-")));
//		logger.info(month);
		
		//迴圈:從最早開始有營收收入統計表年度98年開始到2020年
//		for(int y=98; y<(endYear+1); y++) {//年
//			for(int m=0; m<13; m++) {
//				if(y==98 && m<12) {
//					continue;
//				}
				try {
					//url設定
					String time = 98 + "_" + 12;
//					String time = y + "_" + m;
					String url2 = url.replace("#@", time);
					logger.info(url2);
//					logger.info("::::result::::");
					//爬蟲
					String result = HttpClientService.sendHttpsGet(url2, null,"html");
//					logger.info(result);
					new AllCompanyMonthlyRevenuePage(result).getRevenueInfo();
//					logger.info("==========================================");
					Thread.sleep(3000);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
//		}
//	}
	
}
