package stock_test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import stock.crawler.HttpClientService;
import stock.crawler.dataFormat.CompanyMonthlyRevenuePageInfo;
import stock.entity.CrawledTWStock;
import stock.repos.TWStockDAO;

//爬取頁面:https://mops.twse.com.tw/mops/web/t05st10_ifrs
//
public class CrawlerMonthlyRevenue {
	
	private static final Logger logger = LogManager.getLogger(CrawlerMonthlyRevenue.class);
	
	@Test//獲取最新月營收
	public void getLatestMonthlyRevenuePageInfo() {
		
		String co_id = "";
		String co_name = "";
		//時間
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date today = new Date();
		String date = sdf.format(today);
		//要擷取的頁面
		String url = "https://mops.twse.com.tw/mops/web/t05st10_ifrs";
		//請求參數名稱
		Object[] params = new Object[] {"encodeURIComponent","step","firstin","off","keyword4","code1","TYPEK2"
				,"checkbtn","queryName","inpuType","TYPEK","isnew","co_id","year","month"};
		
		//從DB撈出所有股票代號後，一一查詢最新月營收
		String config = "spring-mvc.xml";
		ApplicationContext ac = new ClassPathXmlApplicationContext(config);
		TWStockDAO dao = ac.getBean("twstockDAO",TWStockDAO.class);
		List<CrawledTWStock> list = dao.getAll();
		for(CrawledTWStock li : list) {
			System.out.print("======= "+ li.getTw_stock_id()+" ======= ");
			String result = null;
			co_id = li.getTw_stock_id();
			if(Integer.valueOf(co_id)<1228) {
				System.out.println("跳過");
				continue;
			}
			System.out.println("");
			co_name = li.getTw_stock_name();
			Object[] values = new Object[] {"1","1","1","1","","","","","co_id","co_id","all","true",co_id,"",""};
			List<NameValuePair> paramsList = HttpClientService.getParams(params, values);
			try {
				System.out.println("delay@@");
				Thread.sleep(5000);
				result = HttpClientService.sendHttpsPost(url,paramsList);
//				System.out.print(result);
				result = result.substring(result.indexOf("yearmonth="));
				String[] data  = new CompanyMonthlyRevenuePageInfo(result,co_id,co_name).getRevenueInfo(); 
//				for(int i=0; i<data.length; i++) {
//					System.out.println("data["+i+"]="+data[i]);
//				}
				System.out.print("date.substring(0,3)===>"+date.substring(0,3));
				System.out.print("date.substring(3, 6)===>"+date.substring(3,5));
				if( dao.findLastestMonthlyRevenueCount(co_id, data[data.length-3], data[data.length-2]) >0) {
					System.out.print(co_id+"已經有資料，跳過~~");
					continue;
				}
				String insertResult = dao.saveStockMonthlyRevenue(data,date);
				System.out.print("===>"+insertResult);
			}  catch (Exception e) {
				e.printStackTrace();
			}
		}//所有股票代號
			
	}
}
