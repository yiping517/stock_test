package stock_test;

import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import stock.entity.TWStock;
import stock.entity.User;
import stock.repos.AccountDAO;
import stock.repos.TWStockDAO;
import stock.service.AccountService;

public class TestCase {
	
	private static final Logger logger = LogManager.getLogger(TestCase.class);
	
	private TWStockDAO dao;
	private AccountService as;
	private AccountDAO a;
	private DataSource ds;
	
	/**
	 * 不論運行test?，這個@Before所修飾的方法都會在該測試方法運行前先執行。
	 */
	@Before
	public void init() {
		String config="spring-mvc.xml";
		ApplicationContext ac = new ClassPathXmlApplicationContext(config);
		dao = ac.getBean("twstockDAO",TWStockDAO.class);
		as = ac.getBean("accountService",AccountService.class);
		a = ac.getBean("accountDAO",AccountDAO.class);
		ds = ac.getBean("ds",DataSource.class);
	}
	
	@Test//測試log4j2，參考:https://www.scalyr.com/blog/maven-log4j2-project/
	public void testLog4j() {
		logger.info("測試log4j");
	}
	
	@Test//測試TWStockDAOImpl的delete()
	public void test8() {
		System.out.println(dao.delete("9"));
	}
	
	@Test//測試TWStockDAOImpl的modify()
	public void test7() {
		TWStock ts = dao.findById("9");
		ts.setBoss("試試");
		System.out.println(dao.modify(ts));
	}
	
	@Test//測試TWStockDAOImpl的findById()
	public void test6() {
		TWStock ts = dao.findById("120");
		System.out.println(ts);
	}
	
	@Test//測試TWStockDAOImpl的findAll()
	public void test5() {
//		String config="spring-mvc.xml";
//		ApplicationContext ac = new ClassPathXmlApplicationContext(config);
//		TWStockDAO dao = ac.getBean("twstockDAO",TWStockDAO.class);
		
//		List<TWStock> list 	= dao.findAll();
//		for(int i=0; i<list.size(); i++) {
//			System.out.println(list.get(i));
//		}
	}
	
	@Test//測試JdbcTemplate的使用 : TWStockDAOImpl
	public void test4() {
//		String config="spring-mvc.xml";
//		ApplicationContext ac = new ClassPathXmlApplicationContext(config);
//		TWStockDAO dao = ac.getBean("twstockDAO",TWStockDAO.class);
		TWStock s = new TWStock();
		s.setTwStockId("9");
		s.setTwStockName("測試");
		s.setStockType("9");
		s.setSetUpTime("2020-09-09");
		s.setBoss("阿平");
		s.setCapital("100億");
		s.setDataTime("2020-09-09");
//		dao.save(s);
	}
	
	@Test//測試AccountService
	public void test3() {
//		String config="spring-mvc.xml";
//		ApplicationContext ac = new ClassPathXmlApplicationContext(config);
//		AccountService as = ac.getBean("accountService",AccountService.class);
		User user = as.checkLogin("ftest1","Aa1234567890!");
		String cname = user.getCname();
		System.out.println(cname);
	}
	
	@Test//測試AccountDAO
	public void test2() throws SQLException {
//		String config="spring-mvc.xml";
//		ApplicationContext ac = new ClassPathXmlApplicationContext(config);
		/**
		 * DataSource是一個接口。
		 * BasicDataSource是一個實現該接口的具體類。
		 */
//		AccountDAO a = ac.getBean("accountDAO",AccountDAO.class);
		User user = a.findByAccount("ftest2");
		System.out.println(user);
	}
	
	@Test//測試連接池
	public void test1() throws SQLException {
//		String config="spring-mvc.xml";
//		ApplicationContext ac = new ClassPathXmlApplicationContext(config);
		/**
		 * DataSource是一個接口。
		 * BasicDataSource是一個實現該接口的具體類。
		 */
//		DataSource ds = ac.getBean("ds",DataSource.class);
		System.out.println(ds.getConnection());
	}

}
