package stock_test;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import stock.controller.RevenueCrawlerController;

public class testRevenueCrawlerController {
	
	RevenueCrawlerController mrc;
	
	@Before
	public void init2() {
//		System.out.println("測試如果方法名不叫init會不會執行");//=>會
		String config = "spring-mvc.xml";
		ApplicationContext ac = new ClassPathXmlApplicationContext(config);
		mrc = ac.getBean("rcController",RevenueCrawlerController.class);
		
	}
	
	@Test
	public void testCrawleAllTWStocksMonthlyRevenue() {
		System.out.println("testRevenueCrawlerController");
		mrc.crawleAllTWStocksMonthlyRevenue();
	}
	
	@Test//測試執行controller
	public void testRevenueCrawlerController() {
		System.out.println("testRevenueCrawlerController");
		mrc.crawleStockLatestMonthlyRevenue();
	}
}
