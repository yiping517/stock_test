package stock_test;

import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import stock.crawler.HttpClientService;
import stock.crawler.dataFormat.StockDailyDealFormat;
import stock.entity.CrawledTWStock;
import stock.repos.TWStockDAO;
import stock.repos.TWStockDAOImpl;

//璣ゅ把σ : 璣ゅ把σ:https://www.twse.com.tw/en/page/trading/exchange/STOCK_DAY.html
//呼 : https://www.twse.com.tw/zh/page/trading/exchange/STOCK_DAY.html
public class StockDailyDealCrawler {
	
	private static final Logger logger = LogManager.getLogger(StockDailyDealCrawler.class);
	
		@Test//盢腹┾传跑计
		public void testGetInfoFromWebPage2() {
			
			//砞﹚
			String url = "https://www.twse.com.tw/exchangeReport/STOCK_DAY?";
			Object[] params = new Object[] {"repsonse","date","stockNo"};//把计嘿
			ApplicationContext ac = new ClassPathXmlApplicationContext("spring-mvc.xml");
			TWStockDAO dao = ac.getBean("twstockDAO",TWStockDAO.class);
			
			//┮Τ布腹
			List<CrawledTWStock> list = dao.getAll();
			for(CrawledTWStock stock : list) {
				String stock_id = stock.getTw_stock_id();
				for(int year=2010; year<2020; year++) {
					for(int month=1; month<13; month++) {
						String searchTime, m ;
						if(month<10) {
							m = "0"+month;
							searchTime = ""+year+m+"01";
							m = ""+year+"-"+m;
						}else {
							searchTime = ""+year+month+"01";
							m = ""+year+"-"+month;
						}
						logger.info("VVVVVVVVVV stock_id="+stock_id+" VVVVVVVVVVVVVVVVVVV");
						logger.info("VVVVVVVVVV "+searchTime.substring(0, searchTime.length()-2)+" VVVVVVVVVVVVVVVVVVV");
						//喷靡
						Long cnt = dao.getStockDailyDealCount(stock_id, m);
						if(cnt>0) {
							continue;
						}
						
						//把计
						Object[] values = new Object[] {"json",searchTime,stock_id};//把计
						List<NameValuePair> paramsList = HttpClientService.getParams(params, values);
						
						try {
							Thread.sleep(10000);
							//祇癳HttpsGet
							String result = null;
							result = HttpClientService.sendHttpsGet(url,paramsList,"json");
//							logger.info(result);
							if(result.length()>1) {
								List<String[]> r =  new StockDailyDealFormat(result,stock_id).processPageJson();
								dao.saveStockDailyDeal(r);
							}
						} catch (Exception e) {
							e.printStackTrace();
							logger.error(e.getMessage());
						}
						
						
					}
				}
			}
		}
	
	
	//璣ゅ把σ : 璣ゅ把σ:https://www.twse.com.tw/en/page/trading/exchange/STOCK_DAY.html
	//呼 : https://www.twse.com.tw/zh/page/trading/exchange/STOCK_DAY.html
	@Test
	public void testGetInfoFromWebPage() {
		
		String url = "https://www.twse.com.tw/exchangeReport/STOCK_DAY?";
		String stock_id ="1101";
		
		ApplicationContext ac = new ClassPathXmlApplicationContext("spring-mvc.xml");
		TWStockDAO tsDAO = ac.getBean("twstockDAO",TWStockDAO.class);
		
		//把计嘿
		Object[] params = new Object[] {"repsonse","date","stockNo"};
		
		//把计
		Object[] values = new Object[] {"json","20100131",stock_id};
		//把计癸禜
		List<NameValuePair> paramsList = HttpClientService.getParams(params, values);
		
		//祇癳HttpsGet
		String result = null;
		result = HttpClientService.sendHttpsGet(url,paramsList,"json");
//		System.out.println(result);
		
		if(result.length()>1) {
			List<String[]> r =  new StockDailyDealFormat(result,stock_id).processPageJson();
//			tsDAO.saveStockDailyDeal(r);
		}
		
	}
}
