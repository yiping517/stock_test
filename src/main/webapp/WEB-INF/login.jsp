<%@page pageEncoding="UTF-8" contentType="text/html; charset=utf-8"%>
<!DOCTYPE html>
<head>
	<meta charset="UTF-8"/>
	<title>登入頁面</title>
	<style>
		
		body{
			/*background-image:url('pics/stock.jpg');*/
			background-size:cover;
			text-align:center;
			/*opacity:0.1;*/
		}
		.form{
			opacity:1;
			font-weight:bold;
		}
		
		
	</style>
</head>
<body>
	<form action="login.do" method="post" class="form">
		<span>${fail_message}</span><br/>	
		帳號:<input type="text" name="id"/><br/>
		密碼:<input type="password" name="pwd"/><br/>
		<input type="submit" value="登入"/>
	</form>
</body>
</html>