package stock.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class TWStock implements Serializable{
	
	private String twStockId;
	private String twStockName;
	private String stockType;
	private String setUpTime;
	private String boss;
	private String capital;
	private String dataTime;
	public String getTwStockId() {
		return twStockId;
	}
	public void setTwStockId(String twStockId) {
		this.twStockId = twStockId;
	}
	public String getTwStockName() {
		return twStockName;
	}
	public void setTwStockName(String twStockName) {
		this.twStockName = twStockName;
	}
	public String getStockType() {
		return stockType;
	}
	public void setStockType(String stockType) {
		this.stockType = stockType;
	}
	public String getSetUpTime() {
		return setUpTime;
	}
	public void setSetUpTime(String setUpTime) {
		this.setUpTime = setUpTime;
	}
	public String getBoss() {
		return boss;
	}
	public void setBoss(String boss) {
		this.boss = boss;
	}
	public String getCapital() {
		return capital;
	}
	public void setCapital(String capital) {
		this.capital = capital;
	}
	public String getDataTime() {
		return dataTime;
	}
	public void setDataTime(String dataTime) {
		this.dataTime = dataTime;
	}
	@Override
	public String toString() {
		return "Stock [twStockId=" + twStockId + ", twStockName=" + twStockName + ", stockType=" + stockType
				+ ", setUpTime=" + setUpTime + ", boss=" + boss + ", capital=" + capital + ", dataTime=" + dataTime
				+ "]";
	}
	
}
