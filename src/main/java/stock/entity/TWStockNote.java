package stock.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class TWStockNote implements Serializable {
		
	public TWStockNote(String user_id, String tw_stock_id, String keyw, String max, String min, String note_content,
			String alarm, String update_time) {
		super();
		this.user_id = user_id;
		this.tw_stock_id = tw_stock_id;
		this.keyw = keyw;
		this.max = max;
		this.min = min;
		this.note_content = note_content;
		this.alarm = alarm;
		this.update_time = update_time;
	}

	private String user_id;
	private String tw_stock_id;
	private String keyw;
	private String max;
	private String min;
	private String note_content;
	private String alarm;
	private String update_time;
	
	public TWStockNote() {}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getTw_stock_id() {
		return tw_stock_id;
	}

	public void setTw_stock_id(String tw_stock_id) {
		this.tw_stock_id = tw_stock_id;
	}

	public String getKeyw() {
		return keyw;
	}

	public void setKeyw(String keyw) {
		this.keyw = keyw;
	}

	public String getMax() {
		return max;
	}

	public void setMax(String max) {
		this.max = max;
	}

	public String getMin() {
		return min;
	}

	public void setMin(String min) {
		this.min = min;
	}

	public String getNote_content() {
		return note_content;
	}

	public void setNote_content(String note_content) {
		this.note_content = note_content;
	}

	public String getAlarm() {
		return alarm;
	}

	public void setAlarm(String alarm) {
		this.alarm = alarm;
	}

	public String getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}

	@Override
	public String toString() {
		return "TWStockNote [user_id=" + user_id + ", tw_stock_id=" + tw_stock_id + ", keyw=" + keyw + ", max=" + max
				+ ", min=" + min + ", note_content=" + note_content + ", alarm=" + alarm + ", update_time="
				+ update_time + "]";
	}
	
}
