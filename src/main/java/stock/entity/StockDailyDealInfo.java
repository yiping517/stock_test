package stock.entity;

import java.io.Serializable;

//璣ゅ把σ:https://www.twse.com.tw/en/page/trading/exchange/STOCK_DAY.html
public class StockDailyDealInfo implements Serializable {
	
	private String date;
	private String trade_volume;//Θユ计
	private String trade_value;//Θユ肂
	private String opening_price;//秨絃基
	private String highest_price;//程蔼基
	private String lowest_price;//程基
	private String closing_price;//Μ絃基
	private String change;//害禴基畉
	private String transaction;//Θユ掸计
	
	public StockDailyDealInfo() {}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTrade_volume() {
		return trade_volume;
	}

	public void setTrade_volume(String trade_volume) {
		this.trade_volume = trade_volume;
	}

	public String getTrade_value() {
		return trade_value;
	}

	public void setTrade_value(String trade_value) {
		this.trade_value = trade_value;
	}

	public String getOpening_price() {
		return opening_price;
	}

	public void setOpening_price(String opening_price) {
		this.opening_price = opening_price;
	}

	public String getHighest_price() {
		return highest_price;
	}

	public void setHighest_price(String highest_price) {
		this.highest_price = highest_price;
	}

	public String getLowest_price() {
		return lowest_price;
	}

	public void setLowest_price(String lowest_price) {
		this.lowest_price = lowest_price;
	}

	public String getClosing_price() {
		return closing_price;
	}

	public void setClosing_price(String closing_price) {
		this.closing_price = closing_price;
	}

	public String getChange() {
		return change;
	}

	public void setChange(String change) {
		this.change = change;
	}

	public String getTransaction() {
		return transaction;
	}

	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}

	@Override
	public String toString() {
		return "StockDailyDealInfo [date=" + date + ", trade_volume=" + trade_volume + ", trade_value=" + trade_value
				+ ", opening_price=" + opening_price + ", highest_price=" + highest_price + ", lowest_price="
				+ lowest_price + ", closing_price=" + closing_price + ", change=" + change + ", transaction="
				+ transaction + "]";
	}
	
}
