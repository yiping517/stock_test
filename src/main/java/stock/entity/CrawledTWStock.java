package stock.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class CrawledTWStock implements Serializable {
	private String stock_type;
	private String tw_stock_id;
	private String tw_stock_name;
	private String tw_stock_fullname;
	private String boss;
	private String set_up_time;
	private String capital;
	private String type1_date;
	private String type2_date;
	private String type3_date;
	private String total_shares;
	private String private_placement_shares;
	private String preferred_stock_shares;
	private String last_update_date;
	
	public CrawledTWStock(String stock_type, String tw_stock_id, String tw_stock_name, String tw_stock_fullname,
			String boss, String set_up_time, String capital, String type1_date, String type2_date, String type3_date,
			String total_shares, String private_placement_shares, String preferred_stock_shares,
			String last_update_date) {
		super();
		this.stock_type = stock_type;
		this.tw_stock_id = tw_stock_id;
		this.tw_stock_name = tw_stock_name;
		this.tw_stock_fullname = tw_stock_fullname;
		this.boss = boss;
		this.set_up_time = set_up_time;
		this.capital = capital;
		this.type1_date = type1_date;
		this.type2_date = type2_date;
		this.type3_date = type3_date;
		this.total_shares = total_shares;
		this.private_placement_shares = private_placement_shares;
		this.preferred_stock_shares = preferred_stock_shares;
		this.last_update_date = last_update_date;
	}
	
	public CrawledTWStock() {}
	
	public String getStock_type() {
		return stock_type;
	}
	public void setStock_type(String stock_type) {
		this.stock_type = stock_type;
	}
	public String getTw_stock_id() {
		return tw_stock_id;
	}
	public void setTw_stock_id(String tw_stock_id) {
		this.tw_stock_id = tw_stock_id;
	}
	public String getTw_stock_name() {
		return tw_stock_name;
	}
	public void setTw_stock_name(String tw_stock_name) {
		this.tw_stock_name = tw_stock_name;
	}
	public String getTw_stock_fullname() {
		return tw_stock_fullname;
	}
	public void setTw_stock_fullname(String tw_stock_fullname) {
		this.tw_stock_fullname = tw_stock_fullname;
	}
	public String getBoss() {
		return boss;
	}
	public void setBoss(String boss) {
		this.boss = boss;
	}
	public String getSet_up_time() {
		return set_up_time;
	}
	public void setSet_up_time(String set_up_time) {
		this.set_up_time = set_up_time;
	}
	public String getCapital() {
		return capital;
	}
	public void setCapital(String capital) {
		this.capital = capital;
	}
	public String getType1_date() {
		return type1_date;
	}
	public void setType1_date(String type1_date) {
		this.type1_date = type1_date;
	}
	public String getType2_date() {
		return type2_date;
	}
	public void setType2_date(String type2_date) {
		this.type2_date = type2_date;
	}
	public String getType3_date() {
		return type3_date;
	}
	public void setType3_date(String type3_date) {
		this.type3_date = type3_date;
	}
	public String getTotal_shares() {
		return total_shares;
	}
	public void setTotal_shares(String total_shares) {
		this.total_shares = total_shares;
	}
	public String getPrivate_placement_shares() {
		return private_placement_shares;
	}
	public void setPrivate_placement_shares(String private_placement_shares) {
		this.private_placement_shares = private_placement_shares;
	}
	public String getPreferred_stock_shares() {
		return preferred_stock_shares;
	}
	public void setPreferred_stock_shares(String preferred_stock_shares) {
		this.preferred_stock_shares = preferred_stock_shares;
	}
	public String getLast_update_date() {
		return last_update_date;
	}
	public void setLast_update_date(String last_update_date) {
		this.last_update_date = last_update_date;
	}
	
}
