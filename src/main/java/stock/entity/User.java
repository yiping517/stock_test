package stock.entity;

import java.io.Serializable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;


@Component
public class User implements Serializable {
	
	private static final Logger logger = LogManager.getLogger(User.class);
	
	private String id;
	private String cname;
	private String pwd;
	private String email;
	private String phone;
	private String userIdentity;
	private String lastLoginTime;
	private String userAllow;
	
	public User() {
		logger.info("User()");
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getUserIdentity() {
		return userIdentity;
	}

	public void setUserIdentity(String userIdentity) {
		this.userIdentity = userIdentity;
	}

	public String getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getUserAllow() {
		return userAllow;
	}

	public void setUserAllow(String userAllow) {
		this.userAllow = userAllow;
	}

	@Override
	public String toString() {
		return "UserEntity [id=" + id + ", cname=" + cname + ", pwd=" + pwd + ", email=" + email + ", phone=" + phone
				+ ", userIdentity=" + userIdentity + ", lastLoginTime=" + lastLoginTime + ", userAllow=" + userAllow
				+ "]";
	}
	
}
