package stock.crawler;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

/**
 * 參考網頁 : https://ixyzero.com/blog/archives/4649.html?fbclid=IwAR22LXnhxhQxLKQA3_WbNkCyET6J4gsSB4N6UZvM1vstWGwDXQH9RTrClgw
 * @author sarah
 *
 */
public class HttpClientService {
	
//	private static final Logger LOGGER = LoggerFactory.getLogger(HttpClientService.class);

	private static final int SUCCESS_CODE = 200;
	
	/**
	 * 發送post請求
	 */
	public static String sendPost(String url, List<NameValuePair> nameValuePairList) {
		
		CloseableHttpClient client = null;
		CloseableHttpResponse response = null;
//		String urlTarget = "https://mops.twse.com.tw/mops/web/t05st03";
		
		try {
			client = HttpClients.createDefault();
			HttpPost post = new HttpPost(url);
			StringEntity entity = new UrlEncodedFormEntity(nameValuePairList,"UTF-8");
			post.setEntity(entity);
			post.setHeader(new BasicHeader("Content-Type","application/x-www-form-urlencoded; charset=utf-8"));
			post.setHeader(new BasicHeader("Accept", "text/plain;charset=utf-8"));
			response = client.execute(post);
			int statusCode = response.getStatusLine().getStatusCode();
			if(SUCCESS_CODE == statusCode) {
				String result = EntityUtils.toString(response.getEntity(), "UTF-8");
				System.out.println("sendPost中的"+result);
				return result;
			}else {
//				LOGGER.error("HttpClientService-statusCode：{}",statusCode);
			}
		} catch (Exception e) {
			e.printStackTrace();
//			 LOGGER.error("HttpClientService-Exception：{}", e);
		} finally {
			
			try {
				response.close();
				client.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	
	/**
	 * 組織請求參數
	 */
	public static List<NameValuePair> getParams(Object[] params, Object[] values){
		
		//校驗參數合法性
		boolean flag = params.length>0 && values.length>0 && params.length == values.length;
		if(flag) {
			List<NameValuePair> list = new ArrayList<>();
			for(int i=0; i<params.length; i++) {
				list.add(new BasicNameValuePair(params[i].toString(),values[i].toString()));
			}
			return list;
		}else {
//			LOGGER.error("HttpClientService-errorMsg:{}","請求參數為空/參數長度不一致");
		}
		
		return null;
	}
	
	/**
	 * 2020/10/16因為遇到爬回來的內容中文亂碼，所以重構的方法
	 * @param url
	 * @param nameValuePairList
	 * @return
	 */
	public static String sendHttpsGet(String url, List<NameValuePair> nameValuePairList, String header,String responseEncoding){
		
		String result = "";
		CloseableHttpClient client = null;
		CloseableHttpResponse response = null;
		PoolingHttpClientConnectionManager connManager;
		
		try {
			connManager = ConnectionManagerBuilder();
			client = HttpClients.custom().setConnectionManager(connManager).build();
			URIBuilder uriBuilder = new URIBuilder(url);
//			if(nameValuePairList!=null) {
//				uriBuilder.addParameters(nameValuePairList);
//			}
			HttpGet httpGet = new HttpGet(uriBuilder.build());
			
			if("json".equals(header)) {
				httpGet.setHeader(new BasicHeader("Content-Type", "application/json; charset=utf-8"));
				httpGet.setHeader("Accept", "text/javascript;charset=utf-8");
			}else if("html".equals(header)) {
				httpGet.setHeader(new BasicHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8"));
//				httpGet.setHeader(new BasicHeader("Content-Type", "text/html; charset=utf-8"));
				httpGet.setHeader("Accept", "text/plain;charset=utf-8");
			}
			
			response = client.execute(httpGet);
			int statusCode = response.getStatusLine().getStatusCode();
			if(SUCCESS_CODE==statusCode) {
				HttpEntity entity = response.getEntity();
				
				//編碼
				if(responseEncoding !=null ) {
					result = EntityUtils.toString(entity,"big5");
				}else {
					result = EntityUtils.toString(entity,"UTF-8");
				}
			}
			
		}  catch (Exception e) {
			e.printStackTrace();
		}finally {
			
			if(response!= null && client!=null ) {
				try {
					response.close();
					client.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}
	
	
	/**
	 * 
	 * @param url
	 * @param nameValuePairList
	 * @return
	 */
	public static String sendHttpsGet(String url, List<NameValuePair> nameValuePairList, String header){
		
		return sendHttpsGet(url, nameValuePairList, header, null);
	}
	
	
	public static String sendHttpsPost(String url, List<NameValuePair> list) throws KeyManagementException, NoSuchAlgorithmException, ClientProtocolException, IOException {
//		System.out.println("sendHttpsPost");
		CloseableHttpClient client = null;
		CloseableHttpResponse response = null;
		
		try {
			PoolingHttpClientConnectionManager connManager = ConnectionManagerBuilder();
			client = HttpClients.custom().setConnectionManager(connManager).build();
			HttpPost post = new HttpPost(url);
			StringEntity entity = new UrlEncodedFormEntity(list,"UTF-8");
			post.setEntity(entity);
			post.setHeader(new BasicHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8"));
			post.setHeader(new BasicHeader("Accept", "text/plain;charset=utf-8"));
			response = client.execute(post);
			int statusCode = response.getStatusLine().getStatusCode();
//			System.out.println("statusCode="+statusCode);
			if(SUCCESS_CODE==statusCode) {
				String result = EntityUtils.toString(response.getEntity(),"UTF-8");
				return result;
			}else {
				System.out.println("SUCCESS_CODE!=200");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			response.close();
			client.close();
		}
		return null;
	}
	
	public static PoolingHttpClientConnectionManager ConnectionManagerBuilder() throws NoSuchAlgorithmException, KeyManagementException {
//		System.out.println("ConnectionManagerBuilder");
		
		SSLContext sslContext = SSLContext.getInstance("TLS");
		//實現X509TrustManager接口，用於繞過驗證
		X509TrustManager trustManager = new X509TrustManager() {

			@Override
			public void checkClientTrusted(X509Certificate[] chain, String authType) 
					throws CertificateException {
				
			}

			@Override
			public void checkServerTrusted(X509Certificate[] chain, String authType) 
					throws CertificateException {
				
			}

			@Override
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}
			
		};
		
		sslContext.init(null, new TrustManager[] {trustManager}, null);
		
		//設置協議http和https對應的處理socket鏈接工廠的對象
		Registry<ConnectionSocketFactory> socketFactoryRegistry  = 
				RegistryBuilder.<ConnectionSocketFactory> create()
				.register("http", PlainConnectionSocketFactory.INSTANCE)
				.register("https", new SSLConnectionSocketFactory(sslContext)).build();
//		System.out.println("ConnectionManagerBuilder中的:"+socketFactoryRegistry);
		PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
		return connManager;
	}
}
