package stock.crawler.dataFormat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AllCompanyMonthlyRevenuePage {

	private static final Logger logger = LogManager.getLogger(AllCompanyMonthlyRevenuePage.class);
	private String crawledPage;
	
	public AllCompanyMonthlyRevenuePage() {}
	public AllCompanyMonthlyRevenuePage(String crawledPage) {
		super();
		this.crawledPage = crawledPage.replaceAll(" ", "").replaceAll("　", "");
//		logger.info(crawledPage);
//		logger.info("====================================");
	}
	
	/**
	 * 從頁面爬值、存入DB
	 * @return
	 */
	public String[] getRevenueInfo() {
		
		String[] revenueInfo = new String[13];
		String[] tables = crawledPage.split("<table");
		for(int i=0; i<tables.length; i++) {
//			logger.info(tables[i]);
			
			//rows
			String[] rows = tables[i].split("<tr");
			for(int r=0; r<rows.length; r++) {
//				logger.info("=====> "+rows[r]);
				
				String[] datas = rows[r].toLowerCase().split("<td");
				//排除合計列
//				if(datas.length < 12) {
//					logger.info("非個股資料列:"+rows[r]);
//					continue;
//				}
				for(int d=0; d<datas.length; d++) {
					logger.info("data["+d+"]="+datas[d]);
					//1:個股代號、2:公司名稱、3:當月營收、4:上月營收、5:去年當月營收、6:上月比較增減%
					//7:去年同月增減%、8:當月累積營收、9:去年累計營收、10:前期比較增減%、11:備註
					
					
				}
				
				logger.info("、、、、、、、、、、、、、、、、、、");
			}
			
			logger.info("-----------------------------------------------");
		}
		
		return revenueInfo;
	}
}
