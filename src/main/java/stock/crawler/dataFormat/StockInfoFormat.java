package stock.crawler.dataFormat;

//針對頁面https://mops.twse.com.tw/mops/web/t05st03的格式整理
public class StockInfoFormat {
	
	//爬到的頁面
	private String page;
	
	public StockInfoFormat() {
		super();
	}

	public StockInfoFormat(String page) {
		super();
		this.page = page;
	}
	
	/**
	 * 首先移除多於段落
	 */
	public String[] removeUselessParagraph() {
		
		//留取script是table的該部分內容
		int tableH = page.indexOf("<!_co_id");
		page = page.substring(tableH);
		int end = page.indexOf("<script");
		page = page.substring(0, end).trim().replaceAll("\n", "").replaceAll("\r", "").trim().replaceAll(" ", "");
		
		//移除style
		page = page.replaceAll("style='text-align:left!important;'", "")
				.replaceAll("style='text-align:center!important;font-family:arial;'", "");
//		System.out.println(page);
//		System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
		
		//取公司股票代號
		int co_idStart = page.indexOf("=");
		String co_id = page.substring(co_idStart+1, co_idStart+5);
		System.out.println("股票代號:"+co_id+"、");
		
		//取公司名稱
		int co_nameStart = page.indexOf(")");
		int co_nameEnd = page.indexOf("</span>　公司提供</b>");
		if(co_nameStart > co_nameEnd) {
			co_nameStart = page.indexOf("本資料由　<spanstyle='color:blue;'>");
			int leng = "本資料由　<spanstyle='color:blue;'>".length();
			co_nameStart = co_nameStart + leng -1;
		}
		String co_name = page.substring(co_nameStart+1,co_nameEnd);
		System.out.println("公司名稱:"+co_name+"、");
		
		//公司全名
		int co_fNameStart = page.indexOf("公司名稱");
		int length = "公司名稱</th><tdclass='lColor'>".length();
		int co_fNameEnd = page.indexOf("</td><thcolspan=2class='dColornowrap'>總機");
		String co_fName = page.substring(co_fNameStart+length, co_fNameEnd);
		System.out.println("公司全稱:"+co_fName+"、");
		
		//董事長
		String boss = "";
		if(page.contains("董事長")) {
			int bossStart = page.indexOf("董事長</th><tdclass='lColor'>");
			length = "董事長</th><tdclass='lColor'>".length();
			boss = page.substring(bossStart+length, 
					page.indexOf("</td><thcolspan=2class='dColornowrap'>總經理"));
			System.out.println("董事長:"+boss+"、");
		}else {
			System.out.println("?!!! No董事長資訊!! 、");
		}
		
		//公司成立日期
		int start_dateStart = page.indexOf("公司成立日期</th><tdclass='lColor'>");
		length = "公司成立日期</th><tdclass='lColor'>".length();
		int start_dateEnd = page.indexOf("</td><thcolspan=2class='dColornowrap'>營利事業統一");
		String start_date = page.substring(start_dateStart+length, start_dateEnd);
//		System.out.println("公司成立日期:"+start_date+"、");
		start_date = changeTaiwanDateToWesternDate(start_date);//調整日期格式
		System.out.println("調整後-公司成立日期:"+start_date+"、");
		
		//實收資本額
		int capitalStart = page.indexOf("實收資本額</th><tdclass='lColor'>");
		length = "實收資本額</th><tdclass='lColor'>".length();
		String capital = page.substring((capitalStart+length),page.indexOf("</td><thcolspan=2class='dColornowrap'>上市日期"));
		capital = capital.replaceAll("元", "").replaceAll(",", "");
		System.out.println("實收資本額:"+capital+"、");
		
		String stockType="1";//預設是"上市"
		
		//上市日期
		int stock_dateStart = page.indexOf("上市日期</th><tdclass='lColor'colspan='2'>");
		length = "上市日期</th><tdclass='lColor'colspan='2'>".length();
		String stock_date = page.substring((stock_dateStart+length), page.indexOf("</td><tr><thcolspan=2class='dColornowrap'>上櫃"));
//		System.out.println("上市日期:"+stock_date+"、");
		stock_date = changeTaiwanDateToWesternDate(stock_date);
		System.out.println("調整後的上市日期:"+stock_date+"、");
		
		//若上市日期是0，則為上櫃類型
		if("".equals(stock_date)) {
			stockType = "2";//上櫃
		}
		
		//上櫃日期
		int stock2_dateStart = page.indexOf("上櫃日期</th><tdclass='lColor'>");
		length = "上櫃日期</th><tdclass='lColor'>".length();
		String stock2_date = page.substring((stock2_dateStart+length), page.indexOf("</td><thcolspan=2class='dColornowrap'>興櫃"));
		stock2_date = changeTaiwanDateToWesternDate(stock2_date);
		System.out.println("調整後的上櫃日期:"+stock2_date+"、");
		
		System.out.println("stock_date是否為'':"+ ("".equals(stock_date)));
		System.out.println("stock2_date是否為'':"+ ("".equals(stock2_date)));
		
		//若上日期是0，則類型為興櫃
		if( ("".equals(stock_date)) && ("".equals(stock2_date)) ) {
			stockType = "3";//興櫃
		}
		
		//興櫃日期
		int stock3_dateStart = page.indexOf("興櫃日期</th><tdclass='lColor'colspan='2'>");
		length = "興櫃日期</th><tdclass='lColor'colspan='2'>".length();
		String stock3_date = page.substring((stock3_dateStart+length), page.indexOf("</td><tr><thcolspan=2class='dColornowrap'>公開發行日期"));
		stock3_date = changeTaiwanDateToWesternDate(stock3_date);
		System.out.println("調整後的興櫃日期:"+stock3_date+"、");
		
		System.out.println("股票類型:"+stockType+"、");
		
		//已發行普通股數或TDR原股發行股數
		int stockStart = page.indexOf("已發行普通股數或TDR原股發行股數</th><tdclass='lColor'>");
		length = "已發行普通股數或TDR原股發行股數</th><tdclass='lColor'>".length();
		String stock = page.substring((stockStart+length),page.indexOf("</td><thcolspan=2class='dColornowrap'>特別股"));
//		System.out.println("已發行普通股數或TDR原股發行股數:"+stock+"、");
		String[] stocks = separatePrivateStockFromOriginal(stock);
		stock = stocks[0];//總股數
		System.out.println("已發行普通股數或TDR原股發行總股數:"+stock+"、");
		System.out.println("私募股數:"+stocks[1]+"、");
		
		//特別股
		int specialStart = page.indexOf("特別股</th><tdclass='lColor'colspan='2'>");
		length = "特別股</th><tdclass='lColor'colspan='2'>".length();
		String special = page.substring((specialStart+length),page.indexOf("</td><tr><thcolspan=2class='dColornowrap'>普通股盈餘分派或虧損撥補頻率</th>"));
		special = special.replaceAll("股", "");
		System.out.println("特別股:"+special+"、");
		System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
		
		String[] result = {stockType,co_id,co_name,co_fName,boss,start_date,capital,stock_date,stock2_date,stock3_date,stock,stocks[1],special};
		return result;
		
	}
	
	/**
	 * 拆出私募股數
	 * @param originalStock
	 * @return
	 */
	public String[] separatePrivateStockFromOriginal(String originalStock) {
		String[] stocks = new String[2];
		if(originalStock.contains(",") || originalStock.contains("股")) {
			originalStock = originalStock.replaceAll(",", "").replaceAll("股", "");
		}
		if(originalStock.contains("(含私募")) {
			stocks[0] = originalStock.substring(0, originalStock.indexOf("("));
			stocks[1] = originalStock.substring(originalStock.indexOf("募")+1,originalStock.lastIndexOf(")"));
		}
		return stocks;
	}
	
	/**
	 * 移除數字型字串中的,
	 * @param numberString
	 * @return
	 */
	public String removeCommaInNumberString(String numberString) {
		if(numberString.contains(",")) {
			numberString = numberString.replaceAll(",", "");
//			System.out.println("移除數值中的, : "+numberString);
		}
		if(numberString.contains("元")) {
			numberString = numberString.replaceAll(",", "");
//			System.out.println("移除單位元 : "+numberString);
		}
		return numberString;
	}
	
	/**
	 * 將民國年轉換成西元年度
	 * @param taiwanDate
	 * @return
	 */
	public String changeTaiwanDateToWesternDate(String taiwanDate) {
		String westernDate = "";
		if(taiwanDate.matches("[0-9]{2}/[0-9]{2}/[0-9]{2}") || taiwanDate.matches("[0-9]{3}/[0-9]{2}/[0-9]{2}") ){
//			System.out.println("進行時間格式調整");
//			System.out.println(start_date.substring(0, 2));
			int year = 1911 + Integer.valueOf(taiwanDate.substring(0,taiwanDate.indexOf("/")));
			westernDate = year + "-" + taiwanDate.substring(taiwanDate.indexOf("/")+1,taiwanDate.lastIndexOf("/")) + "-" + taiwanDate.substring(taiwanDate.length()-2);
		}
		return westernDate;
	}
	
}
