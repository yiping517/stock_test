package stock.crawler.dataFormat;


import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import stock.repos.TWStockDAOImpl;

public class StockDailyDealFormat {
	
	private static final Logger logger = LogManager.getLogger(TWStockDAOImpl.class);
	private String pageJson;
	private String stock_id;
	
	public StockDailyDealFormat() {}

	public StockDailyDealFormat(String pageJson,String stock_id) {
		super();
		this.pageJson = pageJson;
		this.stock_id = stock_id;
//		logger.info(pageJson);
	}
	
	public List<String[]> processPageJson() {
		
		List<String[]> stockDailyDeals = new ArrayList<String[]>();
		
		//成功，參考:
		JsonObject jo = JsonParser.parseString(pageJson).getAsJsonObject();
//		JsonArray ja = jo.getAsJsonArray("title");
		Gson gson = new Gson();
//		ArrayList jsonObjList = gson.fromJson(ja, ArrayList.class);
//		System.out.println("====title====="+jsonObjList.toString());
		
		JsonArray ja = jo.getAsJsonArray("data");
		ArrayList<ArrayList> arr = gson.fromJson(ja, ArrayList.class);
		if(arr==null) {
			logger.info("沒有個股成交資訊");
			throw new RuntimeException("沒有個股成交資訊");
		}
		//天數
		for(int i=0; i<arr.size(); i++) {
			
			String[] stockDailyDeal = new String[10];
			stockDailyDeal[0] = stock_id;
			
			//欄位數
			for(int ii=0; ii<arr.get(0).size(); ii++) {
				String value = String.valueOf(arr.get(i).get(ii));
				if(ii==0) {
					value = value.replaceAll("/", "-");
					
					//驗證是否是民國年
					String year = value.substring(0,value.indexOf("-")).trim();
					if(year.length()<4) {
						int y = Integer.valueOf(year) + 1911;
						value = y + value.substring(value.indexOf("-"));
						stockDailyDeal[9]=value;
					}
					
				}else if(ii==1 || ii==2 || ii==8) {
					value = value.replaceAll(",", "");
					stockDailyDeal[ii]=value;
				}else {
					stockDailyDeal[ii]=value;
				}
			}
			stockDailyDeals.add(stockDailyDeal);
			System.out.print("############################");
			
		}
		
		return stockDailyDeals;
	}
}
