package stock.crawler.dataFormat;

import java.io.Serializable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import stock.exception.ApplicationException;
import stock.repos.LogDAO;

@Component
public class CompanyMonthlyRevenuePageInfo implements Serializable {

	private static final Logger logger = LogManager.getLogger(CompanyMonthlyRevenuePageInfo.class);
	private static final long serialVersionUID = 1L;
	private String crawledPage;
	private String stock_id;
	private String stock_name;
	
	@Autowired
	@Qualifier("logDAO")
	private LogDAO logDAO;
	
	public CompanyMonthlyRevenuePageInfo() {}

	public CompanyMonthlyRevenuePageInfo(String crawledPage,String stock_id,String stock_name) {
		super();
		this.crawledPage = crawledPage;
		this.stock_id = stock_id;
		this.stock_name = stock_name;
	}
	
	public String[] getRevenueInfo() {
		
//		if( !(crawledPage.contains("公司提供") )) {
//			logDAO.saveErrlog();
//			return null;
//		}
		String[] revenueInfo = new String[13];
		revenueInfo[2] = "0";
		revenueInfo[3] = "0";
		revenueInfo[9] = "";
		crawledPage = crawledPage.replaceAll(" ", "").replaceAll("　", "").replaceAll("\\r", "").replaceAll("\\n", "").trim();
//		System.out.println(crawledPage);
//		System.out.println("============================");
		crawledPage = crawledPage.substring(0, crawledPage.indexOf("</TD></TR></TABLE>"));
		System.out.println(crawledPage);
		System.out.println("============================");
		
		int start = crawledPage.indexOf("'compID'value='");
		int length = "'compID'value='".length();
		//股票代號
		String pageStockID = "";
		if(crawledPage.contains("compName")) {
			pageStockID = crawledPage.substring(start+length, crawledPage.indexOf("'><inputtype='hidden'name='compName"));
		}else {
			start = crawledPage.indexOf("公司代號</th><th>公司名稱</th><th>&nbsp;</th></tr><trclass='odd'><td>");
			length = "公司代號</th><th>公司名稱</th><th>&nbsp;</th></tr><trclass='odd'><td>".length();
			pageStockID = crawledPage.substring(start+length, start+length+4);
		}
		System.out.println("pageStockID :"+pageStockID);
		
		//驗證股票代號是否一致
		if(! stock_id.equals(pageStockID)) {
			throw new ApplicationException("DB出來的股票代號與page爬到的代號不一致");
		}
		revenueInfo[0] = stock_id;
		
		//公司名稱
		start = crawledPage.indexOf("'compName'align='center'><b>本資料由(");
		length = "'compName'align='center'><b>本資料由(".length();
		String pageStockName = crawledPage.substring(start+length-1, crawledPage.indexOf("公司提供"));
		System.out.println("pageStockName :"+pageStockName);
		String stockName = pageStockName.substring(pageStockName.indexOf(")")+1);
		System.out.println("stockName :"+stockName);
		
		//驗證公司名稱
		if(! stock_name.equals(stockName)) {
			System.out.println("stock_name :"+stock_name);
			throw new ApplicationException("公司名稱不一致");
		}
		
		//資料年度
		String dataYearMonth = crawledPage.substring(crawledPage.indexOf("=")+1, crawledPage.indexOf("--><tr><tdclass='reportCont"));
		System.out.println("dataYearMonth : "+dataYearMonth);
		if(dataYearMonth.length()==5) {
			revenueInfo[revenueInfo.length-3] = dataYearMonth.substring(0,3);//年度
			revenueInfo[revenueInfo.length-2] = dataYearMonth.substring(3);//月份
		}
		
		String[] clips = crawledPage.split("<TR>");
		int i=0;
		for(String clip : clips) {
			System.out.println("i ="+i+"、$$clip: "+clip);
			clip = clip.replaceAll("style='text-align:right!important;'>&nbsp", "");
			if(i<11 && i>2) {
				String item = clip.substring(clip.indexOf(">")+1, clip.indexOf("</TH>"));
				System.out.println("## item: "+item);
				String value = clip.substring(clip.indexOf(";")+1, clip.indexOf("</TD></TR>"));
				System.out.println("## value: "+value);
				if(value.contains(",")) {
					value = value.replaceAll(",", "");
				}
//				revenueInfo[i-2] = value;
				if("本月".equals(item)) {
					revenueInfo[1] = value;
				}else if("去年同期".equals(item)) {
					revenueInfo[4] = value;
				}else if( i==6 && ("增減百分比".equals(item)) ) {
					revenueInfo[5] = value;
				}else if("本年累計".equals(item)) {
					revenueInfo[6] = value;
				}else if("去年累計".equals(item)) {
					revenueInfo[7] = value;
				}else if( i==10 && ("增減百分比".equals(item)) ) {
					revenueInfo[8] = value;
				}
			}
			i++;
		}
		//split的迴圈
		
		return revenueInfo;
	}
}
