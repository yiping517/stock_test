package stock.repos;

import java.util.List;

import stock.entity.CrawledTWStock;
import stock.entity.TWStock;

public interface TWStockDAO {
	
	//透過爬蟲取得的個股每日成交資訊
//	String saveStockDailyDeal(String[] stockDailyDeal);//
	void saveStockDailyDeal(List<String[]> stockDailyDeal);
	
//	String save(TWStock ts);//新股上市、上櫃
	
	//儲存透過爬蟲取得的股票資訊
	String save(CrawledTWStock cts);
	
	//儲存透過爬蟲取得的個股每月營收資訊
	String saveStockMonthlyRevenue(String[] crawledStockMonthlyRevenue,String updateTime);
	
	//更新透過爬蟲取得的所有台股月營收資訊
	String updateAllTWStocksMonthlyRevenue(String[] crawledStockMonthlyRevenue,String updateTime);
	
	Long getStockDailyDealCount(String stockID, String time);
	
//	List<TWStock> findAll();//列出所有股票
	
	List<CrawledTWStock> getAll();
	
	TWStock findById(String id);//根據股票代號找股票
	
	String modify(TWStock s);
	
	String delete(String id);
	
	//查詢個股最新的月營收筆數
	Long findLastestMonthlyRevenueCount(String stockID, String year, String month);
}
