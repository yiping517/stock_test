package stock.repos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import stock.entity.User;

@Repository("accountDAO")
public class AccountDAOJdbcImpl implements AccountDAO{

	@Resource(name="ds")
	private DataSource ds;
	
	@Override
	public User findByAccount(String account) {
		User user = null;
		Connection conn = null;
		try {
			conn = ds.getConnection();
			String sql = "select * from user_info where id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, account);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				user = new User();
				user.setId(rs.getString("id"));
				user.setCname(rs.getString("cname"));
				user.setEmail(rs.getString("email"));
				user.setLastLoginTime(rs.getString("last_login_time"));
				user.setPhone(rs.getString("phone"));
				user.setPwd(rs.getString("pwd"));
				user.setUserAllow(rs.getString("user_allow"));
				user.setUserIdentity(rs.getString("user_identity"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			if(conn!=null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return user;
	}

}
