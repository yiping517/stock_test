package stock.repos;

import stock.entity.User;

public interface AccountDAO {
	
	User findByAccount(String account);
}
