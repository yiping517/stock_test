package stock.repos;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;

public class BasicJdbcDAOImpl implements BasicDAO{

	static final Logger logger = LogManager.getLogger(BasicJdbcDAOImpl.class);

	@Autowired
	@Qualifier("jdbcTemplate")
	JdbcTemplate jt;
}
