package stock.repos;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.stereotype.Repository;

@Repository("logDAO")
public class LogDAOImpl extends BasicJdbcDAOImpl implements LogDAO{

	@Override
	public String saveErrlog(String userID, String className, String methodName, String parameters, String errMessage) {
		
		logger.info("saveErrlog");
		
		//time
		Date now = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time = sdf.format(now);
		logger.info("time="+time);
		
		String sql = "insert into stockTest_errlog("
				+ "happen_time,user_id,happen_class,happen_method,happen_parameter,"
				+ "error_message) "
				+ "values(?,?,?,?,?,?)";
		Object[] args = {time,userID,className,methodName,parameters,errMessage};
		int result = jt.update(sql, args);
		
		if(result>0) {
			logger.info("success:成功插入一筆紀錄到stockTest_errlog");
			return "success:成功插入一筆紀錄到stockTest_errlog";
		}
		logger.error("fail:插入紀錄到stockTest_errlog時失敗");
		return "fail:插入紀錄到stockTest_errlog時失敗";
	}

}
