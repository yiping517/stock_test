package stock.repos;

public interface LogDAO {
	
	String saveErrlog(String userID, String className, String methodName, String parameters, String errMessage);
}
