package stock.repos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import stock.entity.CrawledTWStock;
import stock.entity.TWStock;

@Repository("twstockDAO")
public class TWStockDAOImpl implements TWStockDAO{
	
	private static final Logger logger = LogManager.getLogger(TWStockDAOImpl.class);
//	@Resource(name="jdbcTemplate")
	@Autowired
	@Qualifier("jdbcTemplate")
	private JdbcTemplate jt;
	
	//該table已淘汰。由crawled_tw_stock_info取代之。
	/*
	@Override
	public String save(TWStock ts) {
		String dataTime = "";
		String sql = "insert into tw_stock_info values(?,?,?,?,?,?,?)";
		Object[] args = {ts.getTwStockId(),ts.getTwStockName(),ts.getStockType(),ts.getSetUpTime(),
						ts.getBoss(),ts.getCapital(),dataTime};
		int result = jt.update(sql, args);
		if(result>0) {
			return "success，成功insert一筆資料進tw_stock_info";
		}
		return "fail";
	}
	*/
	//該table已淘汰。由crawled_tw_stock_info取代之。
	/*
	@Override
	public List<TWStock> findAll() {
		String sql = "select * from tw_stock_info order by tw_stock_id";
		return jt.query(sql, new TWStockRowMapper());
	}
	*/
	/**
	 * 內部類。
	 * 封裝了對ResultSet的處理。
	 */
	class TWStockRowMapper implements RowMapper<TWStock>{

		/**
		 * 這個方法的作用 : 告訴JdbcTemplate，如何將一條紀錄轉換成一個實體對象。
		 * index : 正在被處理的紀錄的下標
		 */
		@Override
		public TWStock mapRow(ResultSet rs, int index) throws SQLException {
			TWStock ts = new TWStock();
			ts.setTwStockId(rs.getString("tw_stock_id"));
			ts.setTwStockName(rs.getString("tw_stock_name"));
			ts.setStockType(rs.getString("stock_type"));
			ts.setSetUpTime(rs.getString("set_up_time"));
			ts.setBoss(rs.getString("boss"));
			ts.setCapital(rs.getString("capital"));
			ts.setDataTime(rs.getString("data_time"));
			return ts;
		}
	}
	
	@Override
	public List<CrawledTWStock> getAll() {
		String sql = "select * from crawled_tw_stock_info order by tw_stock_id";
		return jt.query(sql, new CrawledTWStockRowMapper());
	}
	
	/**
	 * 內部類。
	 * 封裝了對ResultSet的處理。
	 */
	class CrawledTWStockRowMapper implements RowMapper<CrawledTWStock>{

		/**
		 * 這個方法的作用 : 告訴JdbcTemplate，如何將一條紀錄轉換成一個實體對象。
		 * index : 正在被處理的紀錄的下標
		 */
		@Override
		public CrawledTWStock mapRow(ResultSet rs, int index) throws SQLException {
			CrawledTWStock ts = new CrawledTWStock();
			ts.setStock_type(rs.getString("stock_type"));
			ts.setTw_stock_id(rs.getString("tw_stock_id"));
			ts.setTw_stock_name(rs.getString("tw_stock_name"));
			ts.setTw_stock_fullname(rs.getString("tw_stock_fullname"));
			ts.setBoss(rs.getString("boss"));
			ts.setSet_up_time(rs.getString("set_up_time"));
			ts.setCapital(rs.getString("capital"));
			ts.setType1_date(rs.getString("type1_date"));
			ts.setType2_date(rs.getString("type2_date"));
			ts.setType3_date(rs.getString("type3_date"));
			ts.setTotal_shares(rs.getString("total_shares"));
			ts.setPrivate_placement_shares(rs.getString("private_placement_shares"));
			ts.setPreferred_stock_shares(rs.getString("preferred_stock_shares"));
			ts.setLast_update_date(rs.getString("last_update_date"));
			return ts;
		}
	}


	@Override
	public TWStock findById(String id) {
		String sql = "select * from tw_stock_info where tw_stock_id =? ";
		Object[] args = {id};
		TWStock ts ;
		try {
			ts = jt.queryForObject(sql, args, new TWStockRowMapper());
		}catch(EmptyResultDataAccessException e) {
			return null;
		}
		return ts;
	}

	@Override
	public String modify(TWStock s) {
		String sql = "update tw_stock_info set tw_stock_name=?,stock_type=?,"
				+ "set_up_time=?,boss=?,capital=?,data_time=? where tw_stock_id=?";
		Object[] args = {s.getTwStockName(),s.getStockType(),s.getSetUpTime(),
				s.getBoss(),s.getCapital(),s.getDataTime(),s.getTwStockId()};
		int result = jt.update(sql, args);
		if(result>0) {
			return "success，修改成功!";
		}
		return "fail，修改失敗QQ";
	}

	@Override
	public String delete(String id) {
		String sql = "delete from tw_stock_info where tw_stock_id=?";
		Object[] args = {id};
		int result = jt.update(sql, args);
		if(result>0) {
			return "success，資料刪除成功!";
		}
		return "fail，資料刪除失敗QQ";
	}

	@Override
	public String save(CrawledTWStock cts) {
		
		String sql = "insert into crawled_tw_stock_info(stock_type,tw_stock_id,tw_stock_name,tw_stock_fullname,"
				+ "boss,set_up_time,capital,type1_date,type2_date,type3_date,total_shares,"
				+ "private_placement_shares,preferred_stock_shares,last_update_date)"
				+ " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Object[] args = {cts.getStock_type(),cts.getTw_stock_id(),cts.getTw_stock_name(),cts.getTw_stock_fullname(),
				cts.getBoss(),cts.getSet_up_time(),cts.getCapital(),cts.getType1_date(),cts.getType2_date(),cts.getType3_date(),
				cts.getTotal_shares(),cts.getPrivate_placement_shares(),cts.getPreferred_stock_shares(),cts.getLast_update_date()};
		int result=0;
		try {
			result = jt.update(sql, args);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		if(result>0) {
			return "success:insert成功";
		}
		return "fail:insert失敗";
	}

	/**
	 * 儲存透過爬蟲取得的個股每月營收資訊
	 */
	@Override
	public String saveStockMonthlyRevenue(String[] crawledStockMonthlyRevenue, String updateTime) {
		String sql = "insert into crawled_monthly_revenue("
				+ "tw_stock_id,"//1.台股代號
				+ "this_month,"//2.月營收
				+ "last_month,"//3.上月營收
				+ "month_change_percentage,"//4.與上月營收增減百分比
				+ "last_year,"//5.去年當月營收
				+ "year_change_percentage,"//6.去年同月增減百分比
				+ "this_year_cumulative,"//7.本月累計
				+ "last_year_cumulative,"//8.去年累計
				+ "year_cumulative_change_percentage,"//9.累計營收增減百分比
				+ "pag_note,"//10.備註
				+ "revenue_year,"//11.資料年度
				+ "revenue_month,"//12資料月份
				+ "update_time) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
		Object[] args = {
				crawledStockMonthlyRevenue[0],crawledStockMonthlyRevenue[1],crawledStockMonthlyRevenue[2],
				crawledStockMonthlyRevenue[3],crawledStockMonthlyRevenue[4],crawledStockMonthlyRevenue[5],
				crawledStockMonthlyRevenue[6],crawledStockMonthlyRevenue[7],crawledStockMonthlyRevenue[8],
				crawledStockMonthlyRevenue[9],crawledStockMonthlyRevenue[10],crawledStockMonthlyRevenue[11],
				updateTime
		};
		int result=0;
		try {
			result = jt.update(sql, args);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		if(result>0) {
			return "success:insert成功";
		}
		return "fail:insert失敗";
		
	}

	/**
	 * 個股每日成交資訊
	 */
	@Override
//	public String saveStockDailyDeal(String[] stockDailyDeal) {
	public void saveStockDailyDeal(List<String[]> stockDailyDeals) {
		String sql = "insert into crawled_stock_daily_deal("
				+ "tw_stock_id,"
				+ "trade_volume,"
				+ "trade_value,"
				+ "opening_price,"
				+ "highest_price,"
				+ "lowest_price,"
				+ "closing_price,"
				+ "stock_change,"
				+ "stock_daily_transaction,"
				+ "data_date) values(?,?,?,?,?,?,?,?,?,?)";
		
		for(int i=0; i<stockDailyDeals.size(); i++) {
			
			Object[] args = {
					stockDailyDeals.get(i)[0],stockDailyDeals.get(i)[1],stockDailyDeals.get(i)[2],
					stockDailyDeals.get(i)[3],stockDailyDeals.get(i)[4],stockDailyDeals.get(i)[5],
					stockDailyDeals.get(i)[6],stockDailyDeals.get(i)[7],stockDailyDeals.get(i)[8],
					stockDailyDeals.get(i)[9]};
			
			int result=0;
			try {
				result = jt.update(sql, args);
				logger.info("success:"+stockDailyDeals.get(i)[9]+" insert成功");
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public Long getStockDailyDealCount(String stockID, String time) {
		String sql = "select count(*) as cnt from crawled_stock_daily_deal where "
				+ "tw_stock_id=? and data_date like ?";
		time = time+"%";
		Object[] args = {stockID,time};
		List<Map<String,Object>> list = jt.queryForList(sql,args);
		Long cnt = (Long)list.get(0).get("cnt");
		return cnt;
	}

	@Override
	public Long findLastestMonthlyRevenueCount(String stockID, String year, String month) {
		String sql = "select count(*) as cnt from crawled_monthly_revenue where "
				+ "tw_stock_id=? and revenue_year=? and revenue_month=?";
		Object[] args = {stockID,year,month};
		List<Map<String,Object>> list = jt.queryForList(sql,args);
		Long cnt = (Long)list.get(0).get("cnt");
		return cnt;
	}

	/**
	 * 更新透過爬蟲取得的所有台股月營收資訊
	 */
	@Override
	public String updateAllTWStocksMonthlyRevenue(String[] crawledStockMonthlyRevenue, String updateTime) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
