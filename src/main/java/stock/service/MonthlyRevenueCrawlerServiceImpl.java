package stock.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import stock.exception.ApplicationException;
import stock.repos.TWStockDAO;

@Service("mrcService")
public class MonthlyRevenueCrawlerServiceImpl extends BaseServiceImpl implements MonthlyRevenueCrawlerService {

	@Autowired
	@Qualifier("twstockDAO")
	TWStockDAO twstockDAO;
	
	/**
	 * 
	 */
	@Override
	public String[] getStockLastestMonthlyRevenueInfo(String crawledPage,String stock_id,String stock_name) {

		//判斷網頁格式。有些網頁含多間公司。需在這裡先排除&記錄到errlog表中
		if( !(crawledPage.contains("公司提供") )) {
			String parameters = "stock_id="+stock_id+";stock_name="+stock_name+";頁面爬蟲資訊檔crawledPage";
			logDAO.saveErrlog("admin","MonthlyRevenueCrawlerServiceImpl","getStockLastestMonthlyRevenueInfo",parameters,"該頁面與其他頁面不同");
			throw new RuntimeException("網頁結構不同");
		}
		
		String[] revenueInfo = new String[13];
		revenueInfo[2] = "0";
		revenueInfo[3] = "0";
		revenueInfo[9] = "";
		
		//移除網頁空白
		crawledPage = crawledPage.replaceAll(" ", "").replaceAll("　", "").replaceAll("\\r", "").replaceAll("\\n", "").trim();
		crawledPage = crawledPage.substring(0, crawledPage.indexOf("</TD></TR></TABLE>"));
		logger.info(crawledPage);
		System.out.println("============================");
		
		int start = crawledPage.indexOf("'compID'value='");
		int length = "'compID'value='".length();
		//股票代號
		String pageStockID = "";
		if(crawledPage.contains("compName")) {
			pageStockID = crawledPage.substring(start+length, crawledPage.indexOf("'><inputtype='hidden'name='compName"));
		}else {
			start = crawledPage.indexOf("公司代號</th><th>公司名稱</th><th>&nbsp;</th></tr><trclass='odd'><td>");
			length = "公司代號</th><th>公司名稱</th><th>&nbsp;</th></tr><trclass='odd'><td>".length();
			pageStockID = crawledPage.substring(start+length, start+length+4);
		}
		System.out.println("pageStockID :"+pageStockID);
		
		//驗證股票代號是否一致
		if(! stock_id.equals(pageStockID)) {
			throw new ApplicationException("DB出來的股票代號與page爬到的代號不一致");
		}
		revenueInfo[0] = stock_id;
		
		//公司名稱
		start = crawledPage.indexOf("'compName'align='center'><b>本資料由(");
		length = "'compName'align='center'><b>本資料由(".length();
		String pageStockName = crawledPage.substring(start+length-1, crawledPage.indexOf("公司提供"));
		System.out.println("pageStockName :"+pageStockName);
		String stockName = pageStockName.substring(pageStockName.indexOf(")")+1);
		System.out.println("stockName :"+stockName);
		
		//驗證公司名稱
		if(! stock_name.equals(stockName)) {
			System.out.println("stock_name :"+stock_name);
			throw new ApplicationException("公司名稱不一致");
		}
		
		//資料年度
		String dataYearMonth = crawledPage.substring(crawledPage.indexOf("=")+1, crawledPage.indexOf("--><tr><tdclass='reportCont"));
		System.out.println("dataYearMonth : "+dataYearMonth);
		if(dataYearMonth.length()==5) {
			revenueInfo[revenueInfo.length-3] = dataYearMonth.substring(0,3);//年度
			revenueInfo[revenueInfo.length-2] = dataYearMonth.substring(3);//月份
		}
		
		String[] clips = crawledPage.split("<TR>");
		int i=0;
		for(String clip : clips) {
			System.out.println("i ="+i+"、$$clip: "+clip);
			clip = clip.replaceAll("style='text-align:right!important;'>&nbsp", "");
			if(i<11 && i>2) {
				String item = clip.substring(clip.indexOf(">")+1, clip.indexOf("</TH>"));
				System.out.println("## item: "+item);
				String value = clip.substring(clip.indexOf(";")+1, clip.indexOf("</TD></TR>"));
				System.out.println("## value: "+value);
				if(value.contains(",")) {
					value = value.replaceAll(",", "");
				}
//				revenueInfo[i-2] = value;
				if("本月".equals(item)) {
					revenueInfo[1] = value;
				}else if("去年同期".equals(item)) {
					revenueInfo[4] = value;
				}else if( i==6 && ("增減百分比".equals(item)) ) {
					revenueInfo[5] = value;
				}else if("本年累計".equals(item)) {
					revenueInfo[6] = value;
				}else if("去年累計".equals(item)) {
					revenueInfo[7] = value;
				}else if( i==10 && ("增減百分比".equals(item)) ) {
					revenueInfo[8] = value;
				}
			}
			i++;
		}
		//split的迴圈
		
		return revenueInfo;
	}

	/*
	 DB : 1.台股代號、2.月營收、3.上月營收、4.與上月營收增減百分比、5.去年當月營收、去6.年同月增減百分比、
	      7.本年累計、8.去年累計、9.累計營收增減百分比、10.備註、11.資料年度、12.資料月份、13.資料更新時間
	 * */
	@Override
	public String[] updateAllTWStockLastestMonthlyRevenueInfo(String crawledPage) {
		
		String[] result = new String[12];
		
		//頁面資料擷取
		result[10] = crawledPage.substring(crawledPage.indexOf("司")+1, crawledPage.indexOf("年"));
		result[11] = crawledPage.substring(crawledPage.indexOf("年")+1, crawledPage.indexOf("月"));
		
		String[] infos =  crawledPage.split("<tralign=right");
		for(int i=1; i<infos.length/80; i++) {
			
			//移除多餘的css
			if(infos[i].contains("nowrap")) {
				infos[i] = infos[i].replaceAll("nowrap", "");
			}
			if(infos[i].contains("align=center")) {
				infos[i] = infos[i].replaceAll("align=center", "");
			}
			if(infos[i].contains("align=left")) {
				infos[i] = infos[i].replaceAll("align=left", "");
			}
//			logger.info("infos["+i+"]="+infos[i]);
//			logger.info("----------------------------------");
			
			String[] datas = infos[i].split("</td>");
			for(int ii=0; ii<datas.length; ii++) {
				
//				logger.info("=========>datas["+ii+"]="+datas[ii]);
				datas[ii] = datas[ii].replaceAll("<td>", "");
				if(ii==0) {//公司代號
					result[0] = datas[ii];
				}else if(ii==2) {//當月營收
					result[1] = datas[ii];
				}else if(ii==3) {//上月營收
					result[2] = datas[ii];
				}else if(ii==4) {//去年當月營收
					result[4] = datas[ii];
				}else if(ii==5) {//上月比較增減
					result[3] = datas[ii];
				}else if(ii==6) {//去年同月增減
					result[5] = datas[ii];
				}else if(ii==7) {//當月累計營收
					result[6] = datas[ii];
				}else if(ii==8) {//去年累計營收
					result[7] = datas[ii];
				}else if(ii==9) {//累計營收比較增減
					result[8] = datas[ii];
				}else if(ii==10) {//備註
					result[9] = datas[ii];
				}
				
				
			}
		
			
			//檢驗資料是否已存在:存在->update；不存在->insert
			Long cnt = twstockDAO.findLastestMonthlyRevenueCount(result[0], result[10], result[11]);
			logger.info("股票代號:"+result[0]+"=> cnt="+cnt);
			
		}
		
		return null;
	}

}
