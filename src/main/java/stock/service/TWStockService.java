package stock.service;

import java.util.List;

import stock.entity.CrawledTWStock;
import stock.entity.TWStockNote;

public interface TWStockService {
	
	Long IsLastestMonthlyRevenueExist(String stockID, String year, String month);
	
	/**
	 * 所有個股列總列表
	 */
	List<CrawledTWStock> getAll();
	
	/**
	 * 儲存關於個股的個人筆記
	 */
	 String saveStockNote(TWStockNote note);
	 
	 /**
	  * 找出所有關於個人的筆記
	  */
	 List<TWStockNote> getAllStockNotes(String userID);
	 
	 /**
	  * 儲存個股月營收
	  * @param crawledStockMonthlyRevenue
	  * @param updateTime
	  * @return
	  */
	 String saveStockMonthlyRevenue(String[] crawledStockMonthlyRevenue,String updateTime);
}
