package stock.service;

import stock.entity.User;

public interface AccountService {

	User checkLogin(String id, String pwd);
}
