package stock.service;

public interface MonthlyRevenueCrawlerService {
	
	/**
	 * 更新台灣股票最新營收資訊
	 * @param crawledPage
	 * @param stock_id
	 * @param stock_name
	 * @return
	 */
	String[] updateAllTWStockLastestMonthlyRevenueInfo(String crawledPage);
	
	/**
	 * 獲取個股最新月營收資訊
	 * @param crawledPage
	 * @param stock_id
	 * @param stock_name
	 * @return
	 */
	String[] getStockLastestMonthlyRevenueInfo(String crawledPage,String stock_id,String stock_name);
}
