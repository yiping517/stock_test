package stock.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import stock.entity.CrawledTWStock;
import stock.entity.TWStockNote;
import stock.repos.TWStockDAO;

@Service("twstockService")
public class TWStockServiceImpl implements TWStockService {

	@Resource(name="twstockDAO")
	private TWStockDAO twstockDAO;
	
	/**
	 * 找出所有的台灣股票
	 */
	@Override
	public List<CrawledTWStock> getAll() {
		List<CrawledTWStock> list = twstockDAO.getAll();
		return list;
	}

	/**
	 * 儲存關於個股的個人筆記
	 */
	@Override
	public String saveStockNote(TWStockNote note) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TWStockNote> getAllStockNotes(String userID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long IsLastestMonthlyRevenueExist(String stockID, String year, String month) {
		Long count = twstockDAO.findLastestMonthlyRevenueCount(stockID, year, month);
		return count;
	}

	@Override
	public String saveStockMonthlyRevenue(String[] crawledStockMonthlyRevenue, String updateTime) {
		String result = twstockDAO.saveStockMonthlyRevenue(crawledStockMonthlyRevenue,updateTime);
		return result;
	}

}
