package stock.service;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import stock.entity.User;
import stock.exception.AccountException;
import stock.repos.AccountDAO;

@Service("accountService")
public class AccountServiceImpl implements AccountService{

	@Resource(name="accountDAO")
	private AccountDAO accountDAO;
	
	@Override
	public User checkLogin(String id, String pwd) {
		
		User user = accountDAO.findByAccount(id);
		if(user == null || !(pwd.equals(user.getPwd())) ) {
			throw new AccountException("登入帳密有誤");
		}
		if("F".equals(user.getUserAllow()) ) {
			throw new AccountException("該帳號已被停用");
		}
		return user;
	}

}
