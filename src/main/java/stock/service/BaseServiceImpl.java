package stock.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import stock.repos.LogDAO;

@Service("baseService")
public class BaseServiceImpl implements BaseService {
	
	static final Logger logger = LogManager.getLogger(BaseService.class);
	
	@Autowired
	@Qualifier("logDAO")
	LogDAO logDAO;
	
}
