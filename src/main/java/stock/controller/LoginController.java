package stock.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import stock.entity.User;
import stock.exception.AccountException;
import stock.service.AccountService;

@Controller
public class LoginController extends BaseController {
	
	@Resource(name="accountService")
	private AccountService accountService;
	
	
	/**
	 * 前往登入頁面
	 * @return
	 */
	@RequestMapping("/toLogin.do")
	public String toLogin() {
		
		return "login";
	}
	
	/**
	 * 回首頁
	 * @return
	 */
	@RequestMapping("/toHome.do")
	public String toHome() {
		return "home";
	}
	
	/**
	 * 進行登入動作
	 * @param req
	 * @param session
	 * @return
	 */
	@RequestMapping("/login.do")
	public String login(HttpServletRequest req, HttpSession session) {
		
		String id = req.getParameter("id");
		String pwd = req.getParameter("pwd");
		System.out.println("id:"+id+"、pwd:"+pwd);
		User user = null;
		
		try {
			user = accountService.checkLogin(id, pwd);
			
			//與LoginInterceptor搭配使用
			//將數據綁到session對象上
			session.setAttribute("user", user);
			
		}catch(Exception e) {
			e.printStackTrace();
			if(e instanceof AccountException) {
				req.setAttribute("fail_message", e.getMessage());
				return "login";//默認就是轉發
			}
			return "error";
		}
		return "home";
	}
	
	@RequestMapping("/sign.do")
	public String sign(HttpServletRequest req) {
		return "";
	}
}
