package stock.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.http.NameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import stock.crawler.HttpClientService;
import stock.crawler.dataFormat.AllCompanyMonthlyRevenuePage;
import stock.entity.CrawledTWStock;
import stock.service.MonthlyRevenueCrawlerServiceImpl;
import stock.service.TWStockService;
import stock.utils.DateUtil;

@Controller("rcController")
@RequestMapping("/revenue")
public class RevenueCrawlerController extends BaseController {
	
	private String date = DateUtil.getWesternDate();//時間
	
	@Autowired
	@Qualifier("mrcService")
	private MonthlyRevenueCrawlerServiceImpl mrcService;
	
	@Autowired
	@Qualifier("twstockService")
	private TWStockService twstockService;
	
	/**
	 * 每月
	 */
	@RequestMapping("/allTWStocksLatestMonthlyRevenue")
	@ResponseBody
	public String crawleAllTWStocksMonthlyRevenue() {
		
		logger.info("RevenueCrawlerController中的crawleAllTWStocksMonthlyRevenue()");
		
		String taiwanYearMonth = (Integer.valueOf(date.substring(0, 4)) - 1911) + "_" + Integer.valueOf(date.substring(5,7));
		String[] urls = {"https://mops.twse.com.tw/nas/t21/sii/t21sc03_#@_0.html","https://mops.twse.com.tw/nas/t21/sii/t21sc03_#@_1.html"};
		
		//因為有2個url要執行
		for(String url : urls) {
			try {
				//url設定
				url = url.replace("#@", taiwanYearMonth);
				
				if(url.contains("_0.html")) {
					url = "https://mops.twse.com.tw/nas/t21/sii/t21sc03_109_9_0.html";
				}else {
					url = "https://mops.twse.com.tw/nas/t21/sii/t21sc03_109_9_1.html";
				}
				
				//執行爬蟲
				String result = HttpClientService.sendHttpsGet(url, null,"html","big5").replaceAll(" ", "").replaceAll("　", "").trim();
//				logger.info(result);
				if(result.contains("查無資料")) {
					//寫error表
					throw new RuntimeException("查無資料");
				}
				
				String[] data = mrcService.updateAllTWStockLastestMonthlyRevenueInfo(result);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			
		}
		
		return "success";
	}
	
	
	/**
	 * 每月10後執行的 爬取個股最新當月營收
	 * @return
	 */
	@RequestMapping("/stockLatestMonthlyRevenue")
	@ResponseBody
	public String crawleStockLatestMonthlyRevenue() {
		
		logger.info("RevenueCrawlerController中的crawleStockLatestMonthlyRevenue()");
		
		//參數設定:
		String co_id = "";
		String co_name = "";
//		String date = DateUtil.getWesternDate();//時間
		String url = "https://mops.twse.com.tw/mops/web/t05st10_ifrs";//要擷取的頁面
		//請求參數名稱
		Object[] params = new Object[] {"encodeURIComponent","step","firstin","off","keyword4","code1","TYPEK2"
				,"checkbtn","queryName","inpuType","TYPEK","isnew","co_id","year","month"};
		
		//從DB撈出所有股票代號後，一一查詢最新月營收
		List<CrawledTWStock> list = twstockService.getAll();
		for(CrawledTWStock li : list) {
			System.out.print("======= "+ li.getTw_stock_id()+" ======= ");
			String result = null;
			co_id = li.getTw_stock_id();
//			if(Integer.valueOf(co_id)<1228) {
//				System.out.println("跳過");
//				continue;
//			}
			System.out.println("");
			co_name = li.getTw_stock_name();
			Object[] values = new Object[] {"1","1","1","1","","","","","co_id","co_id","all","true",co_id,"",""};
			List<NameValuePair> paramsList = HttpClientService.getParams(params, values);
			try {
				System.out.println("delay@@");
				Thread.sleep(5000);
				result = HttpClientService.sendHttpsPost(url,paramsList);
				System.out.print(result);
				result = result.substring(result.indexOf("yearmonth="));
				
				//處理中
//				String[] data  = new CompanyMonthlyRevenuePageInfo(result,co_id,co_name).getRevenueInfo(); 
				String[] data =mrcService.getStockLastestMonthlyRevenueInfo(result,co_id,co_name);
				System.out.print("date.substring(0,3)===>"+date.substring(0,3));
				System.out.print("date.substring(3, 6)===>"+date.substring(3,5));
				
				if( twstockService.IsLastestMonthlyRevenueExist(co_id, data[data.length-3], data[data.length-2]) >0) {
					System.out.print(co_id+"已經有資料，跳過~~");
					continue;
				}
				String insertResult = twstockService.saveStockMonthlyRevenue(data,date);
				System.out.print("===>"+insertResult);
				
			}  catch (Exception e) {
				e.printStackTrace();
			}
		}//所有股票代號
		
		return null;
	}
}
