package stock.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import stock.entity.CrawledTWStock;
import stock.service.TWStockService;

@Controller
//@RequestMapping("/stock")
public class TWStockController {

	private static final Logger logger = LogManager.getLogger(TWStockController.class);
	
	public TWStockController() {
		logger.info("TWStockController()");
	}

	@Autowired 
	private TWStockService twsService;
	
	/**
	 * �ӪѦC��
	 * @return
	 */
//	@RequestMapping(value="/list",method=RequestMethod.GET)
	@RequestMapping("/toList.do")
//	@ResponseBody
	public String toListPage(ModelMap map,HttpServletRequest req){
		logger.info("toListPage()");
//		List<CrawledTWStock> list = twsService.getAll();
//		map.addAttribute("twStockList", list);
		return "list";
	}
	
	@RequestMapping("/list.do")
	@ResponseBody
	public Object getAll(ModelMap map,HttpServletRequest req){
		logger.info("getAll()");
		List<CrawledTWStock> list = twsService.getAll();
//		map.addAttribute("twStockList", list);
		return list;
	}
	
}
