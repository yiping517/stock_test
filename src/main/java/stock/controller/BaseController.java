package stock.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.ExceptionHandler;

import stock.service.BaseService;


public class BaseController {
	
	static final Logger logger = LogManager.getLogger(BaseController.class);
	
	@Resource(name="baseService")
	BaseService baseService;
	
	/**
	 * @param e : controller方法所拋出的異常
	 * @return
	 */
	@ExceptionHandler
	public String handlerEx(HttpServletRequest req,Exception e) {
		System.out.println("handlerEx()");
		if(e instanceof NumberFormatException) {
			e.printStackTrace();
			req.setAttribute("msg", "請輸入正確的數字");
		}
		return "formatError";
	}
	
}
