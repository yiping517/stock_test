package stock.aop;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

/**
 * 對業務層進行性能測試
 */
@Aspect
public class PerformanceAspect {
	
	private static final Logger logger = LogManager.getLogger(PerformanceAspect.class);
	
	@Around("bean(*Service)")
	public Object test(ProceedingJoinPoint jp) throws Throwable {
		
		//JoinPoint對象可以獲取目標業務方法的詳細信息:方法簽名、調用參數等
		Signature s = jp.getSignature();
		
		long t1 = System.currentTimeMillis();
		Object val = jp.proceed();
		long t2 = System.currentTimeMillis();
		long t = t2-t1;
		logger.info(s.getName()+"耗時:"+t);
		return val;
		
	}
}
