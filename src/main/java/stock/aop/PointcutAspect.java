package stock.aop;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

//�Ѧ�:https://blog.csdn.net/oKuZuoZhou/article/details/81015310
//@Component
@Aspect
public class PointcutAspect {
	
	private static final Logger logger = LogManager.getLogger(PointcutAspect.class);
	
//	@Before("bean(*Service)")
//	@Before("bean(twstockDAO)")
	public void test() {
		logger.info("PointcutAspect��*Service����");
	}
}
