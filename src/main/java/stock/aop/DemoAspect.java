package stock.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * 創建一個切面組件，就是一個普通的javabean
 */
//@Component 
//將@component註解掉、bean和aop:aspectj-autoproxy配置從spring-aop.xml移到spring-mvc.xml後，就成功了
@Aspect//剛剛怎樣都弄不進來，把pom檔中的runtime註解掉就行了。
public class DemoAspect {
	
	/**
	 * 環繞通知。
	 * 
	 * 方法要求:
	 * 1.必須有返回值Object
	 * 2.必須有參數 ProceedingJoinPoint
	 * 3.必須拋出異常
	 * 4.需在方法中調用ProceedingJoinPoint.proceed()
	 * 5.返回業務方法的返回值
	 * 
	 * @param jp
	 * @return
	 * @throws Throwable
	 */
	@Around("bean(twstockDAO)")
	public Object test5(ProceedingJoinPoint jp) throws Throwable {
		Object val = jp.proceed();
		System.out.println("業務結果:"+val);
		return val;
//		throw new RuntimeException("查詢失敗");
	}
	
	//聲明test方法將在twstockDAO的全部方法之前運行=>攔截到twstockDAO之前
	@Before("bean(twstockDAO)")
	public void test() {
		System.out.println("AOP測試---Before");
	}
	
	@After("bean(twstockDAO)")
	public void test2() {
		System.out.println("AOP測試---After");
	}
	
	@AfterReturning("bean(twstockDAO)")
	public void test3() {
		System.out.println("AOP測試---@AfterReturning");
	}
	
	@AfterThrowing("bean(twstockDAO)")
	public void test4() {
		System.out.println("AOP測試---@AfterThrowing");
	}
}
