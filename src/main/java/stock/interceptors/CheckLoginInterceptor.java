package stock.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import stock.entity.User;

public class CheckLoginInterceptor implements HandlerInterceptor {

	/**
	 * DispatcherServlet在收到請求後，會先調用preHandle方法。
	 * 若此方法的返回值是true，則繼續向後【Interceptor、controller】調用。
	 * 若返回值是false，則中斷請求。即，請求結束，不在繼續調用。
	 * 
	 * DispatcherServer、攔截器&Controller會共享同一個Request、Response對象。
	 * 
	 * handler:Controller的方法對象。與java反射有關，就是處理器的那個方法。
	 * 如:想知道LoginController的方法名、返回值、參數類型是什麼，就可以通過java反射。
	 * 而java反射依賴於這個Object參數。
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		System.out.println("preHandle()");
		Object user = request.getSession().getAttribute("user");
		if(user!=null) {
			return true;
		}
		//重定向到登入頁面
		response.sendRedirect("toLogin.do");
//		return false;
		return true;
	}

	/**
	 * 1.Controller的方法執行完畢，正準備將ModelAndView返回給DispatcherServlet前，執行postHandle方法。
	 * 2.可以在此方法裡，修改處理結果(ModelAndView)
	 */
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		System.out.println("postHandle()");
	}

	/**
	 * ex是Controller所拋出的異常。
	 */
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		System.out.println("afterCompletion()");
	}

}
